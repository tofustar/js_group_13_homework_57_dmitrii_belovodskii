import { Component } from '@angular/core';
import { User } from '../shared/user.model';
import { UsersService } from '../shared/users.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  name = '';
  email = '';
  activity: boolean = false;
  type = '';

  constructor(public usersService: UsersService) {}


  addNewUser() {
    const newUser = new User(this.name, this.email, this.activity, this.type);
    this.usersService.addUser(newUser);
  }
}
